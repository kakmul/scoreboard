package com.scoreboard.apps.activity.apps.referee;

import android.provider.BaseColumns;

import java.io.Serializable;

public class RefereeModel implements  Serializable, BaseColumns {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    private String id;
    private String nama;
    private String email;
    private String phone_number;
    private String password;
    private String photo;
    private String created_at;
    private String update_at;

    public RefereeModel(){

    }

    public RefereeModel(String id, String nama, String email, String phone_number,String password,String photo,String created_at,String update_at) {
        this.id = id;
        this.nama = nama;
        this.email = email;
        this.phone_number = phone_number;
        this.password = password;
        this.photo = photo;
        this.created_at = created_at;
        this.update_at = update_at;
    }



}