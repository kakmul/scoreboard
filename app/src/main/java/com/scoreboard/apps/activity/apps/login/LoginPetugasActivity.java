package com.scoreboard.apps.activity.apps.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.scoreboard.apps.R;
import com.scoreboard.apps.activity.apps.addEvent.AddCompetitionActivity;
import com.scoreboard.apps.activity.apps.addEvent.addEvent;
import com.scoreboard.apps.activity.apps.petugas.TambahEventActivity;
import com.scoreboard.apps.activity.apps.register.RegisterPetugasActivity;
import com.scoreboard.apps.activity.apps.register.RegisterWasitActivity;

public class LoginPetugasActivity extends AppCompatActivity {

    Button loginButton;
    LinearLayout sign_up_for_account;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_petugas);
        loginButton = (Button)findViewById(R.id.loginButton);
        sign_up_for_account = (LinearLayout)findViewById(R.id.sign_up_for_account);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginPetugasActivity.this, TambahEventActivity.class));

            }
        });

        sign_up_for_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginPetugasActivity.this, RegisterPetugasActivity.class));

            }
        });
    }
}
