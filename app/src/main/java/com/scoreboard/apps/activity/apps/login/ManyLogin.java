package com.scoreboard.apps.activity.apps.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.scoreboard.apps.R;
import com.scoreboard.apps.activity.apps.register.RegisterWasitActivity;
import com.scoreboard.apps.activity.apps.score.MatchScoringActivity;
import com.scoreboard.apps.activity.apps.tv.ScoreboardTVActivity;

public class ManyLogin extends AppCompatActivity {
    LinearLayout sign_up_for_account,panitiaButton,tvButton,wasitButton,scoringButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_many_login);

        sign_up_for_account = (LinearLayout)findViewById(R.id.sign_up_for_account);
        panitiaButton = (LinearLayout)findViewById(R.id.panitiaButton);
        tvButton = (LinearLayout)findViewById(R.id.tvButton);
        wasitButton = (LinearLayout)findViewById(R.id.wasitButton);


        scoringButton = (LinearLayout)findViewById(R.id.scoringButton);


        sign_up_for_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ManyLogin.this, RegisterWasitActivity.class));
            }
        });


        panitiaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ManyLogin.this, LoginPetugasActivity.class));
            }
        });


        tvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ManyLogin.this, ScoreboardTVActivity.class));
            }
        });


        wasitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ManyLogin.this, LoginWasitActivity.class));
            }
        });

        scoringButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MatchScoringActivity.class);
                intent.putExtra("match_id","GZyr3JSZCYMOhnEzYBWK");
                v.getContext().startActivity(intent);

            }
        });

    }
}
