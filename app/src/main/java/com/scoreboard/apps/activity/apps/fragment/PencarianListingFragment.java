package com.scoreboard.apps.activity.apps.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Source;
import com.scoreboard.apps.R;

import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.scoreboard.apps.activity.apps.addEvent.model.MatchModel;
import com.scoreboard.apps.activity.apps.scoring.ScoreAdapter;
import com.scoreboard.apps.activity.apps.scoring.ScoreModel;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;


public class PencarianListingFragment extends Fragment {

    public PencarianListingFragment() {
        // Required empty public constructor
    }

    public static PencarianListingFragment newInstance() {
        PencarianListingFragment fragment = new PencarianListingFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    private ViewPager viewPager;
    private ViewPager viewPager_2;
    private LinearLayout layout_dots;
    private LinearLayout layout_dots_2;
    List<ScoreModel> personUtilsList;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter mAdapter;

    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences sharedpreferences;
    private JSONArray ottosData;

    Skeleton skeleton;

    //Firebase
    private FirebaseFirestore db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pencarian_listing, container, false);
        init();
        //layout_dots_2 = (LinearLayout) view.findViewById(R.id.layout_dots_2);
       // viewPager_2 = (ViewPager) view.findViewById(R.id.pager_2);


        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(layoutManager);

        personUtilsList = new ArrayList<>();

        //HistoryPreorderAPI("1234567890");
       // getData();
        getFriendList();

//        adapterImageSlider.setItems(items);
//        adapterImageSlider_2.setItems(items_2);
//        viewPager.setAdapter(adapterImageSlider);
//        viewPager_2.setAdapter(adapterImageSlider_2);
//
//        // displaying selected image first
//        viewPager.setCurrentItem(0);
//        addBottomDots(layout_dots, adapterImageSlider.getCount(), 0);
//        addBottomDots(layout_dots_2, adapterImageSlider_2.getCount(), 0);
//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int pos, float positionOffset, int positionOffsetPixels) {
//            }
//
//            @Override
//            public void onPageSelected(int pos) {
//                addBottomDots(layout_dots, adapterImageSlider.getCount(), pos);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//            }
//        });





        return  view;

    }

    private void init(){
        db = FirebaseFirestore.getInstance();
    }


    private void getFriendList(){
        skeleton = SkeletonLayoutUtils.applySkeleton(recyclerView, R.layout.listing_score_item, 10);
        skeleton.showSkeleton();
        CollectionReference productsRef = db.collection("match");
        Query query = productsRef.whereEqualTo("id_team_away", 1);
        // Source can be CACHE, SERVER, or DEFAULT.
        Source source = Source.CACHE;
        productsRef.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                List<MatchModel> mMissionsList = new ArrayList<>();
                if(task.isSuccessful()){
                    for(QueryDocumentSnapshot document : task.getResult()) {
                        final MatchModel miss = document.toObject(MatchModel.class);
                        mMissionsList.add(miss);
                        final String match_id = document.getId();
                       // Log.e("matchname", String.valueOf(miss.getId_team_away()));
                        //mAdapter = new ScoreAdapter(getActivity(), personUtilsList);
                        //personUtilsList.add(new ScoreModel("1",miss.getName_match(),"087123123","","","","",""));
                        CollectionReference productsRef2 = db.collection("team");
                        Query query2 = productsRef2.whereEqualTo("id", miss.getId_team_away());
                        query2.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                List<MatchModel> mMissionsList = new ArrayList<>();
                                if(task.isSuccessful()){
                                    for(QueryDocumentSnapshot document : task.getResult()) {
                                        MatchModel miss2 = document.toObject(MatchModel.class);
                                        mMissionsList.add(miss2);
                                        Log.e("matchname",document.toString());
                                        personUtilsList.add(new ScoreModel("1",miss.getName_match(),"087123123","","","","","",match_id));
                                        mAdapter = new ScoreAdapter(getActivity(), personUtilsList);
                                        onDataLoaded();
                                    }
                                    recyclerView.setAdapter(mAdapter);
//                    ListView mMissionsListView = (ListView) findViewById(R.id.missionList);
//                    MissionsAdapter mMissionAdapter = new MissionsAdapter(this, mMissionsList);
//                    mMissionsListView.setAdapter(mMissionAdapter);
                                } else {
                                    onDataLoaded();
                                    recyclerView.setVisibility(View.GONE);
                                    Log.d("MissionActivity", "Error getting documents: ", task.getException());
                                }
                            }
                        });
                        onDataLoaded();
                    }
                    recyclerView.setAdapter(mAdapter);
//                    ListView mMissionsListView = (ListView) findViewById(R.id.missionList);
//                    MissionsAdapter mMissionAdapter = new MissionsAdapter(this, mMissionsList);
//                    mMissionsListView.setAdapter(mMissionAdapter);
                } else {
                    onDataLoaded();
                    recyclerView.setVisibility(View.GONE);
                    Log.d("MissionActivity", "Error getting documents: ", task.getException());
                }
            }
        });
    }






    // Example callback that hides skeleton
    private void onDataLoaded() {
        skeleton.showOriginal();
    }





    private void addBottomDots(LinearLayout layout_dots, int size, int current) {
        ImageView[] dots = new ImageView[size];
        layout_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(getActivity());
            int width_height = 15;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(ContextCompat.getColor(getActivity(), R.color.overlay_dark_10), PorterDuff.Mode.SRC_ATOP);
            layout_dots.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[current].setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimaryLight), PorterDuff.Mode.SRC_ATOP);
        }
    }


}
