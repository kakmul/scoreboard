package com.scoreboard.apps.activity.apps.addEvent.model;

public class TeamModel {

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_participant_1() {
        return id_participant_1;
    }

    public void setId_participant_1(int id_participant_1) {
        this.id_participant_1 = id_participant_1;
    }

    public int getId_participant_2() {
        return id_participant_2;
    }

    public void setId_participant_2(int id_participant_2) {
        this.id_participant_2 = id_participant_2;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public String getTeam_nickname() {
        return team_nickname;
    }

    public void setTeam_nickname(String team_nickname) {
        this.team_nickname = team_nickname;
    }

    public String getTeam_type() {
        return team_type;
    }

    public void setTeam_type(String team_type) {
        this.team_type = team_type;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }


    private String country;
    private  String created_at;
    private  String flag;
    private int id;
    private int id_participant_1;
    private int id_participant_2;
    private String logo;
    private String team_name;
    private String team_nickname;
    private String team_type;
    private String updated_at;

    public TeamModel() {}

    public TeamModel(String country,String created_at,String flag,int id,int id_participant_1,
                      int id_participant_2,String logo,String team_name,String team_nickname,String team_type,String updated_at) {
        this.country = country;
        this.created_at = created_at;
        this.flag = flag;
        this.id = id;
        this.id_participant_1 = id_participant_1;
        this.id_participant_2 = id_participant_2;
        this.logo = logo;
        this.team_name = team_name;
        this.team_nickname = team_nickname;
        this.team_type = team_type;
        this.updated_at = updated_at;
    }



}
