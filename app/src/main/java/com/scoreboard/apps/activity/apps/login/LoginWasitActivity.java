package com.scoreboard.apps.activity.apps.login;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseError;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.scoreboard.apps.R;
import com.scoreboard.apps.activity.apps.LoginActivity;
import com.scoreboard.apps.activity.apps.addEvent.AddCompetition;
import com.scoreboard.apps.activity.apps.addEvent.AddCompetitionActivity;
import com.scoreboard.apps.activity.apps.addEvent.addEvent;
import com.scoreboard.apps.activity.apps.referee.RefereeAuth;
import com.scoreboard.apps.activity.apps.referee.RefereeModel;
import com.scoreboard.apps.activity.apps.register.RegisterWasitActivity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;


public class LoginWasitActivity extends AppCompatActivity {
    Button loginButton;
    LinearLayout sign_up_for_account;
    // variable yang merefers ke Firebase Realtime Database
    private DatabaseReference database;
    DatabaseReference mRef;
    long value;
    FirebaseFirestore firestoreDB;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    TextInputEditText emailtxt,passwordtxt;
    KProgressHUD hud;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_wasit);
        firestoreDB = FirebaseFirestore.getInstance();
        loginButton = (Button)findViewById(R.id.loginButton);
        sign_up_for_account = (LinearLayout)findViewById(R.id.sign_up_for_account);

        emailtxt = (TextInputEditText)findViewById(R.id.emailtxt);
        passwordtxt = (TextInputEditText)findViewById(R.id.passwordtxt);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        // mengambil referensi ke Firebase Database
        database = FirebaseDatabase.getInstance().getReference();


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(LoginWasitActivity.this, AddCompetitionActivity.class));
               //  submitBarang(new RefereeModel("1","Mulia Rifai","mul@gmail.com","087738000123","pass123","photo.jpg","2019-12-30","2019-12-30"));
                //Log.e("lalalala","first");




                //fireStore(new RefereeModel("1","Mulia Rifai","mul@gmail.com","087738000123","pass123","photo.jpg","2019-12-30","2019-12-30"));

                //saveTransaction();
                //addTen();


                signIn();



            }
        });

        sign_up_for_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginWasitActivity.this, RegisterWasitActivity.class));

            }
        });
    }


    //fungsi dipanggil ketika proses Authentikasi berhasil
    private void onAuthSuccess(FirebaseUser user) {
        String username = usernameFromEmail(user.getEmail());

        // membuat User admin baru
        writeNewAdmin(user.getUid(), username, user.getEmail());
        // Go to MainActivity
        startActivity(new Intent(LoginWasitActivity.this, addEvent.class));
        finish();
    }

    /*
        ini fungsi buat bikin username dari email
            contoh email: abcdefg@mail.com
            maka username nya: abcdefg
     */
    private String usernameFromEmail(String email) {
        if (email.contains("@")) {
            return email.split("@")[0];
        } else {
            return email;
        }
    }

    private void writeNewAdmin(String userId, String name, String email) {
        RefereeAuth admin = new RefereeAuth(name, email);

        mDatabase.child("admins").child(userId).setValue(admin);
    }


    //fungsi untuk memvalidasi EditText email dan password agar tak kosong dan sesuai format
    private boolean validateForm() {
        boolean result = true;
        if (TextUtils.isEmpty(emailtxt.getText().toString())) {
            emailtxt.setError("Required");
            result = false;

        } else {
            emailtxt.setError(null);
        }

        if (TextUtils.isEmpty(passwordtxt.getText().toString())) {
            passwordtxt.setError("Required");
            result = false;
        } else {
            passwordtxt.setError(null);
        }

        return result;
    }

    //fungsi signin untuk mengkonfirmasi data pengguna yang sudah mendaftar sebelumnya
    private void signIn() {

         hud = KProgressHUD.create(LoginWasitActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Silahkan Tunggu")
                .setDetailsLabel("Sedang Melakukan Login")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        if (!validateForm()) {
           hud.dismiss();
            return;
        }

        //showProgressDialog();
        String email_string = emailtxt.getText().toString();
        String password_string = passwordtxt.getText().toString();

        mAuth.signInWithEmailAndPassword(email_string, password_string)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //hideProgressDialog();

                        if (task.isSuccessful()) {
                            hud.dismiss();
                            onAuthSuccess(task.getResult().getUser());
                        } else {
                            hud.dismiss();
                            Toast.makeText(LoginWasitActivity.this, "Login Gagal",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }



    public void addTen() {
        mRef = FirebaseDatabase.getInstance().getReferenceFromUrl("https://scoreboard-828a5.firebaseio.com/scoreboard/referee");
        //mRef = new Firebase(my_private_url);
        mRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                value = (long) dataSnapshot.getValue();
                value = value + 10;
                dataSnapshot.getRef().setValue(value);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }


    public void saveTransaction(){
//        RefereeModel transaction = new RefereeModel("1","Mulia Rifai","mul@gmail.com","087738000123","pass123","photo.jpg","2019-12-30","2019-12-30");
//        db.getReference().child("users").child("data")
//                .child("transactions").push().setValue(transaction);

       // DocumentReference docRef = db.collection("Users").document("XSPH...");


        FirebaseDatabase database;
        final DatabaseReference usersTable;
        database = FirebaseDatabase.getInstance();
        usersTable = database.getReference("Users");
        /*we work here */
        usersTable.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                    RefereeModel users = new RefereeModel("1","Mulia Rifai","mul@gmail.com","087738000123","pass123","photo.jpg","2019-12-30","2019-12-30");
                    usersTable.child("data").setValue(users);
                   // Log.e("lalalala",users.getNama());
                    Toast.makeText(LoginWasitActivity.this, "Sucessfully", Toast.LENGTH_SHORT).show();
                    finish();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }



    public void fireStore(RefereeModel refereeModel){


        FirebaseFirestore db = FirebaseFirestore.getInstance();
        firestoreDB.collection("referee")
                .add(refereeModel)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(LoginWasitActivity.this,
                                "Event document has been added",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("TAG", "Error adding event document", e);
                        Toast.makeText(LoginWasitActivity.this,
                                "Event document could not be added",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void submitBarang(RefereeModel refereeModel) {
        /**
         * Ini adalah kode yang digunakan untuk mengirimkan data ke Firebase Realtime Database
         * dan juga kita set onSuccessListener yang berisi kode yang akan dijalankan
         * ketika data berhasil ditambahkan
         */
        Log.e("lalalala","kedua");



       // ref.child("personal_information").child(incrementedId).setValue(per)

        database.child("scoreboard").child("referee").push().setValue(refereeModel).addOnSuccessListener(this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Snackbar.make(findViewById(R.id.bt_submit), "Data berhasil ditambahkan", Snackbar.LENGTH_LONG).show();
                Log.e("c","ketiga");

            }
        });
    }


}
