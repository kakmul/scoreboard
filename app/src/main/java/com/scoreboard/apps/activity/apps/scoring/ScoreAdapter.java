package com.scoreboard.apps.activity.apps.scoring;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.scoreboard.apps.R;
import com.scoreboard.apps.activity.apps.score.MatchScoringActivity;

import java.util.List;

public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.ViewHolder> {

    Context context;
    List<ScoreModel> personUtils;
    Skeleton skeleton;
    View itemView;

    public ScoreAdapter(Context context, List personUtils) {
        this.context = context;
        this.personUtils = personUtils;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listing_score_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(personUtils.get(position));
        final ScoreModel pu = personUtils.get(position);
        Log.e("matchname",pu.getTitle());


        holder.eventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //v.getContext().startActivity(new Intent(v.getContext(), ScoreboardTVActivity.class));
                Intent intent = new Intent(v.getContext(), MatchScoringActivity.class);
                intent.putExtra("match_id",pu.getMatch_id());
                v.getContext().startActivity(intent);
            }
        });

        holder.name.setText(pu.getTitle());
        //holder.address.setText(pu.getAddress());
       // holder.status.setText(pu.getStatus());
//        Log.e("exception",pu.getGambar());


//        Picasso.get()
//                .load(pu.getThumbnail())
//                .fit().centerCrop()
//                .into(holder.thumbnail_img, new Callback() {
//                    @Override
//                    public void onSuccess() {
//                        //skeleton.showOriginal();
//                        //  progressBar4.setVisibility(View.GONE);
//                        // gambar_produk.setVisibility(View.VISIBLE);
//                    }
//
//                    @Override
//                    public void onError(Exception e) {
//                        //skeleton.showOriginal();
//                        //  progressBar4.setVisibility(View.VISIBLE);
//                        //   gambar_produk.setVisibility(View.GONE);
//                    }
//                });
//

    }

    @Override
    public int getItemCount() {
        return personUtils.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView name;
        public TextView address;
        public TextView status;
        public ImageView thumbnail_img;
        public CardView eventButton;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView = itemView;
            name = (TextView) itemView.findViewById(R.id.name);
            address = (TextView) itemView.findViewById(R.id.address);

            thumbnail_img = (ImageView) itemView.findViewById(R.id.thumbnail_img);

            // Either use an existing Skeletonlayout
            skeleton = itemView.findViewById(R.id.skeletonLayout);


            eventButton = (CardView)itemView.findViewById(R.id.eventButton);
           // skeleton.showSkeleton();
        }
    }


}