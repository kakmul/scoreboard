package com.scoreboard.apps.activity.apps.addEvent.model;

public class ScoreModel {


    public int getAway_score() {
        return away_score;
    }

    public void setAway_score(int away_score) {
        this.away_score = away_score;
    }

    public Boolean getComplete() {
        return complete;
    }

    public void setComplete(Boolean complete) {
        this.complete = complete;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getHome_score() {
        return home_score;
    }

    public void setHome_score(int home_score) {
        this.home_score = home_score;
    }

    public String getId_match() {
        return id_match;
    }

    public void setId_match(String id_match) {
        this.id_match = id_match;
    }

    public int getSet() {
        return set;
    }

    public void setSet(int set) {
        this.set = set;
    }

    private int away_score;
    private Boolean complete;
    private String duration;
    private int home_score;
    private String id_match;
    private int set;


    public ScoreModel() {}

    public ScoreModel(int away_score,Boolean complete,String duration,int home_score,String id_match,
                      int set) {
        this.away_score = away_score;
        this.complete = complete;
        this.duration = duration;
        this.home_score = home_score;
        this.id_match = id_match;
        this.set = set;
    }


}
