package com.scoreboard.apps.activity.apps.referee;

import com.google.firebase.firestore.IgnoreExtraProperties;

@IgnoreExtraProperties
public class RefereeAuth {

    public String username;
    public String email;

    public RefereeAuth() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public RefereeAuth(String username, String email) {
        this.username = username;
        this.email = email;
    }

}