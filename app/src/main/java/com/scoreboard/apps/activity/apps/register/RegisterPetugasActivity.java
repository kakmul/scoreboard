package com.scoreboard.apps.activity.apps.register;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.scoreboard.apps.R;
import com.scoreboard.apps.activity.apps.login.LoginWasitActivity;
import com.scoreboard.apps.activity.apps.referee.RefereeAuth;
import com.scoreboard.apps.activity.apps.referee.RefereeModel;
import com.scoreboard.apps.utils.Tools;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class RegisterPetugasActivity extends AppCompatActivity {
    AutoCompleteTextView email,password,nama,phone;
    Button email_sign_in_button;

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    KProgressHUD hud;
    FirebaseFirestore firestoreDB;


    //upload image

    private Button uploadButton, UnggahGambar;
    private ImageView ImageContainer;
    private ProgressBar progressBar;

    //Deklarasi Variable StorageReference
    private StorageReference reference;

    //Kode permintaan untuk memilih metode pengambilan gamabr
    private static final int REQUEST_CODE_CAMERA = 1;
    private static final int REQUEST_CODE_GALLERY = 2;
    String namaFile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_petugas);
        firestoreDB = FirebaseFirestore.getInstance();
        initToolbar();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        email_sign_in_button = (Button)findViewById(R.id.email_sign_in_button);
        email = (AutoCompleteTextView)findViewById(R.id.email);
        password = (AutoCompleteTextView)findViewById(R.id.password);
        nama = (AutoCompleteTextView)findViewById(R.id.nama);
        phone = (AutoCompleteTextView)findViewById(R.id.phone);


        //upload
       // uploadButton = (Button) findViewById(R.id.uploadButton);
        ImageContainer = (ImageView)findViewById(R.id.ImageContainer);

//        uploadButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getImage();
//            }
//        });
        progressBar = findViewById(R.id.progressBar);
        //Mendapatkan Referensi dari Firebase Storage
        reference = FirebaseStorage.getInstance().getReference();



        email_sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email_string = email.getText().toString();
                String password_string = password.getText().toString();
//                Log.e("excep",ImageContainer.getTag().toString());

                hud = KProgressHUD.create(RegisterPetugasActivity.this)
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setLabel("Silahkan Tunggu")
                        .setDetailsLabel("Sedang Melalukan Registrasi")
                        .setCancellable(true)
                        .setAnimationSpeed(2)
                        .setDimAmount(0.5f)
                        .show();

                //uploadImage();
                signUp();
            }
        });


    }

    //Method ini digunakan untuk mengupload gambar pada Storage
    private void uploadImage(){
        if (!validateForm()) {
            hud.dismiss();
            return;
        }
        //Mendapatkan data dari ImageView sebagai Bytes
        ImageContainer.setDrawingCacheEnabled(true);
        ImageContainer.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable) ImageContainer.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        //Mengkompress bitmap menjadi JPG dengan kualitas gambar 100%
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] bytes = stream.toByteArray();

        //Lokasi lengkap dimana gambar akan disimpan
        namaFile = UUID.randomUUID()+".jpg";
        String pathImage = "gambar/"+namaFile;
        UploadTask uploadTask = reference.child(pathImage).putBytes(bytes);
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                progressBar.setVisibility(View.GONE);
                signUp();
                //Toast.makeText(RegisterWasitActivity.this, "Uploading Berhasil", Toast.LENGTH_SHORT).show();
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressBar.setVisibility(View.GONE);
                        hud.dismiss();
                        Toast.makeText(RegisterPetugasActivity.this, "Register Gagal", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        progressBar.setVisibility(View.VISIBLE);
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        progressBar.setProgress((int) progress);
                    }
                });
    }


    //Menghandle hasil data yang diambil dari kamera atau galeri untuk ditampilkan pada ImageView
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode){
            case REQUEST_CODE_CAMERA:
                if(resultCode == RESULT_OK){
                    ImageContainer.setVisibility(View.VISIBLE);
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    ImageContainer.setImageBitmap(bitmap);
                    ImageContainer.setTag("sudah");
                }
                break;

            case REQUEST_CODE_GALLERY:
                if(resultCode == RESULT_OK){
                    ImageContainer.setVisibility(View.VISIBLE);
                    Uri uri = data.getData();
                    ImageContainer.setImageURI(uri);
                    ImageContainer.setTag("sudah");

                }
                break;
        }
    }


    private void getImage(){
        CharSequence[] menu = {"Kamera", "Galeri"};
        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setTitle("Upload Image")
                .setItems(menu, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                //Mengambil gambar dari Kemara ponsel
                                Intent imageIntentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(imageIntentCamera, REQUEST_CODE_CAMERA);
                                break;

                            case 1:
                                //Mengambil gambar dari galeri
                                Intent imageIntentGallery = new Intent(Intent.ACTION_PICK,
                                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(imageIntentGallery, REQUEST_CODE_GALLERY);
                                break;
                        }
                    }
                });
        dialog.create();
        dialog.show();
    }


    public void fireStore(RefereeModel refereeModel){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        firestoreDB.collection("crew")
                .add(refereeModel)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
//                        Toast.makeText(RegisterWasitActivity.this,
//                                "Event document has been added",
//                                Toast.LENGTH_SHORT).show();
                        hud.dismiss();
                        startActivity(new Intent(RegisterPetugasActivity.this, LoginWasitActivity.class));
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        hud.dismiss();

                        //Log.w("TAG", "Error adding event document", e);
                        Toast.makeText(RegisterPetugasActivity.this,
                                "Gagal, Silahkan Coba Lagi",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public static String getcurrentDateAndTime(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String formattedDate = simpleDateFormat.format(c);
        return formattedDate;
    }


    //fungsi untuk memvalidasi EditText email dan password agar tak kosong dan sesuai format
    private boolean validateForm() {
        boolean result = true;
        if (TextUtils.isEmpty(email.getText().toString())) {
            email.setError("Required");
            result = false;
        } else {
            email.setError(null);
        }

        if (TextUtils.isEmpty(password.getText().toString())) {
            password.setError("Required");
            result = false;
        } else {
            password.setError(null);
        }

        if (TextUtils.isEmpty(nama.getText().toString())) {
            nama.setError("Required");
            result = false;
        } else {
            nama.setError(null);
        }

        if (TextUtils.isEmpty(phone.getText().toString())) {
            phone.setError("Required");
            result = false;
        } else {
            phone.setError(null);
        }

//        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//            Log.e("auth", String.valueOf(ImageContainer.getDrawable().getConstantState()));
//            if (ImageContainer.getDrawable().getConstantState() == ImageContainer.getContext().getDrawable(R.drawable.ic_photo).getConstantState() ){
//
//                uploadButton.setError("Required");
//                result = false;
//            }
//            else{
//                uploadButton.setError(null);
//            }
//
//        }
//        else {
//            Log.e("auth 2", String.valueOf(ImageContainer.getDrawable().getConstantState()));
//
//            if (ImageContainer.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_photo).getConstantState()){
//                uploadButton.setError("Required");
//                result = false;
//            }
//
//            else{
//                uploadButton.setError(null);
//            }
//        }
//
//        Log.e("auth",ImageContainer.getTag().toString());
//        if(ImageContainer.getTag().toString().equals("ic_photo")) {
//            uploadButton.setError("Required");
//            result = false;
//        } else {
//            uploadButton.setError(null);
//
//        }

        return result;
    }


    //fungsi signin untuk mengkonfirmasi data pengguna yang sudah mendaftar sebelumnya
    private void signIn() {
        if (!validateForm()) {
            return;
        }

        //showProgressDialog();
        String email_string = email.getText().toString();
        String password_string = password.getText().toString();

        mAuth.signInWithEmailAndPassword(email_string, password_string)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //hideProgressDialog();

                        if (task.isSuccessful()) {
                            onAuthSuccess(task.getResult().getUser());
                        } else {
                            Toast.makeText(RegisterPetugasActivity.this, "Sign In Failed",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    //fungsi ini untuk mendaftarkan data pengguna ke Firebase
    private void signUp() {

        if (!validateForm()) {
            hud.dismiss();
            return;
        }
        //showProgressDialog();
        String email_string = email.getText().toString();
        String password_string = password.getText().toString();
        Log.e("auth",email_string);
        Log.e("auth",password_string);
        mAuth.createUserWithEmailAndPassword(email_string, password_string)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //hideProgressDialog();
                        if (task.isSuccessful()) {
                            String nama_string = nama.getText().toString();
                            String phone_string = phone.getText().toString();
                            String email_string = email.getText().toString();
                            String password_string = password.getText().toString();
                            fireStore(new RefereeModel("0",nama_string,email_string,phone_string,password_string,namaFile,getcurrentDateAndTime(),getcurrentDateAndTime()));
                            Log.e("excep","berhasil");

                            onAuthSuccess(task.getResult().getUser());
                        } else {
                            hud.dismiss();
                            Log.e("excep","gagal");
                            Toast.makeText(RegisterPetugasActivity.this, "Pendaftaran Gagal",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    //fungsi dipanggil ketika proses Authentikasi berhasil
    private void onAuthSuccess(FirebaseUser user) {
        String username = usernameFromEmail(user.getEmail());

        // membuat User admin baru
        writeNewAdmin(user.getUid(), username, user.getEmail());
        // Go to MainActivity

    }

    /*
        ini fungsi buat bikin username dari email
            contoh email: abcdefg@mail.com
            maka username nya: abcdefg
     */
    private String usernameFromEmail(String email) {
        if (email.contains("@")) {
            return email.split("@")[0];
        } else {
            return email;
        }
    }

    private void writeNewAdmin(String userId, String name, String email) {
        RefereeAuth admin = new RefereeAuth(name, email);
        mDatabase.child("admins").child(userId).setValue(admin);
    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pendaftaran Sebagai Petugas");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this);
    }
}
