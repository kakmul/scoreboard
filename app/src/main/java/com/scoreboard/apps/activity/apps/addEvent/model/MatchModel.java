package com.scoreboard.apps.activity.apps.addEvent.model;
import com.google.firebase.firestore.IgnoreExtraProperties;
@IgnoreExtraProperties


public class MatchModel {

    private String name_match;
    private  String active;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getField_number() {
        return field_number;
    }

    public void setField_number(int field_number) {
        this.field_number = field_number;
    }

    public int getId_crew() {
        return id_crew;
    }

    public void setId_crew(int id_crew) {
        this.id_crew = id_crew;
    }

    public int getId_referee() {
        return id_referee;
    }

    public void setId_referee(int id_referee) {
        this.id_referee = id_referee;
    }

    public int getId_team_away() {
        return id_team_away;
    }

    public void setId_team_away(int id_team_away) {
        this.id_team_away = id_team_away;
    }

    public int getId_team_home() {
        return id_team_home;
    }

    public void setId_team_home(int id_team_home) {
        this.id_team_home = id_team_home;
    }

    public String getMatch_type() {
        return match_type;
    }

    public void setMatch_type(String match_type) {
        this.match_type = match_type;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }
    private  String match_type;
    private  String created_at;
    private  String date;
    private  int field_number;
    private  int id_crew;
    private  int id_referee;
    private  int id_team_away;
    private  int id_team_home;
    private  String notes;
    private  String update_at;

    public MatchModel() {}

    public MatchModel(String name_match,String created_at,String date,int field_number,int id_crew,
                      int id_referee,int id_team_away,int id_team_home,String notes,String update_at) {
        this.name_match = name_match;
        this.created_at = created_at;
        this.date = date;
        this.field_number = field_number;
        this.id_crew = id_crew;
        this.id_referee = id_referee;
        this.id_team_away = id_team_away;
        this.id_team_home = id_team_home;
        this.update_at = update_at;
    }


    public String getName_match() {
        return name_match;
    }

    public void setName_match(String name_match) {
        this.name_match = name_match;
    }
}