package com.scoreboard.apps.activity.apps.addEvent.model;

public class CompetitionModel {

    private String name_match;
    private  String active;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getField_number() {
        return field_number;
    }

    public void setField_number(int field_number) {
        this.field_number = field_number;
    }

    public int getId_crew() {
        return id_crew;
    }

    public void setId_crew(int id_crew) {
        this.id_crew = id_crew;
    }

    public int getId_referee() {
        return id_referee;
    }

    public void setId_referee(int id_referee) {
        this.id_referee = id_referee;
    }

    public int getId_team_away() {
        return id_team_away;
    }

    public void setId_team_away(int id_team_away) {
        this.id_team_away = id_team_away;
    }

    public int getId_team_home() {
        return id_team_home;
    }

    public void setId_team_home(int id_team_home) {
        this.id_team_home = id_team_home;
    }

    public String getMatch_type() {
        return match_type;
    }

    public void setMatch_type(String match_type) {
        this.match_type = match_type;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }
    private  String match_type;
    private  String created_at;
    private  String date;
    private  int field_number;
    private  int id_crew;
    private  int id_referee;
    private  int id_team_away;
    private  int id_team_home;
    private  String notes;
    private  String update_at;




    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getParticipant_1() {
        return participant_1;
    }

    public void setParticipant_1(String participant_1) {
        this.participant_1 = participant_1;
    }

    public String getParticipant_2() {
        return participant_2;
    }

    public void setParticipant_2(String participant_2) {
        this.participant_2 = participant_2;
    }

    public String getParticipant_photo_1() {
        return participant_photo_1;
    }

    public void setParticipant_photo_1(String participant_photo_1) {
        this.participant_photo_1 = participant_photo_1;
    }

    public String getParticipant_photo_2() {
        return participant_photo_2;
    }

    public void setParticipant_photo_2(String participant_photo_2) {
        this.participant_photo_2 = participant_photo_2;
    }

    public String getTeam_1() {
        return team_1;
    }

    public void setTeam_1(String team_1) {
        this.team_1 = team_1;
    }

    public String getTeam_2() {
        return team_2;
    }

    public void setTeam_2(String team_2) {
        this.team_2 = team_2;
    }


    //addon
    private String category;



    private String participant_1;
    private String participant_2;

    private String participant_photo_1;
    private String participant_photo_2;

    private String team_1;
    private String team_2;



    public CompetitionModel() {}

    public CompetitionModel(String name_match,String created_at,String date,int field_number,int id_crew,
                      int id_referee,int id_team_away,int id_team_home,String notes,String update_at,String category,String pariticpant_name_1,String pariticpant_name_2,
                            String pariticpant_photo_1,String pariticpant_photo_2,String team_1,String team_2) {
        this.name_match = name_match;
        this.created_at = created_at;
        this.date = date;
        this.field_number = field_number;
        this.id_crew = id_crew;
        this.id_referee = id_referee;
        this.id_team_away = id_team_away;
        this.id_team_home = id_team_home;
        this.update_at = update_at;

        //addon
        this.category = category;
        this.participant_1 = pariticpant_name_1;
        this.participant_2 = pariticpant_name_2;
        this.participant_photo_1 = pariticpant_photo_1;
        this.participant_photo_2 = pariticpant_photo_2;

        this.team_1 = team_1;
        this.team_2 = team_2;

    }


    public String getName_match() {
        return name_match;
    }

    public void setName_match(String name_match) {
        this.name_match = name_match;
    }
}